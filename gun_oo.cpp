public class Flags {
	    object green;
	    object red;
	    object yellow;
	    object violet;
	
	    void Flags() {
		this.green = radar(GreenFlag);
		this.red = radar(RedFlag);
		this.yellow = radar(YellowFlag);
		this.violet = radar(VioletFlag);
	}
}

public class Points {
	    point pointX;
	    point pointZ;
	    point pointY;
	
	    void Points() {
		    object conv = radar(Converter);
		    point con  (conv.position.x, conv.position.y, conv.position.z);
		
		    this.pointX.x = con.x + 5;
		    this.pointX.y = con.y + 10;
		    this.pointX.z = con.z;
		    this.pointZ.x = pointX.x - 6;
		    this.pointZ.y = pointX.y;
		    this.pointZ.z = pointX.z;
		    this.pointY.x = con.x + 5;
		    this.pointY.y = con.y - 5;
		    this.pointY.z = con.z;
	}
}

public class Robot {
	    protected point position;
	    
	    void takeOutCell() {
		        grab(EnergyCell);
		        drop(Behind);
		    }
	
	    void stealCell() {
		        grab();
		        drop(EnergyCell);
		    }
	
	    void waitForCompanion(int name, float distance) {
		        object companion;
		        while(true) {
			            companion = radar(name, 0, 360, 0, 40);
			            if(companion != null) {
				                if(distance(this.position, companion.position) <= distance) {
					                    wait(2.3);
					                    break;
					                } else {
					                    wait(0.1);
					                }
				            }
			        }
		    }
	
	    void savePosition(point where) {
		        this.position.x = where.x;
		        this.position.y = where.y;
		        this.position.z = where.z;
		    }
	
	    float getSavedX() {
		        return this.position.x;
		    }
	
	    float getSavedY() {
		        return this.position.y;
		    }
	        
	    float getSavedZ() {
		        return this.position.z;
		    }
	
	    void goFor(int target, point neighbourhood, int dirx, int diry) {
		        goto(search(target, neighbourhood.x + dirx, neighbourhood.y + diry).position);
		        grab();
		    }
	
	    void placeItem(point neighbourhood, int minr, int maxr) {
		        goto(space(neighbourhood, minr, maxr));
		        drop();
		    }
}
        
public class Gun extends Robot {
	
}

extern void object::gun_oo()
{
	object spider = radar(AlienSpider);
	object trap = radar(TargetBot);
	object fly = radar(WingedGrabber);
	
	while(spider != null) {
		turn(direction(spider.position));
		motor(1, 1);
		while(true) {
			if(distance(position, spider.position) < 15) {
				fire(1);
				wait(2);
				//goto(spider.position);
				spider = radar(AlienSpider);
				break;
				
			}
		}
	}
	
	while(trap != null) {
		turn(direction(trap.position));
		motor(1, 1);
		if(distance(position, trap.position) < 20) {
			motor(0, 0);
			fire(1);
			trap = radar(TargetBot);
		}
	}
	
	object conv = radar(Converter);
	
	object WingedG = search(WingedGrabber, conv.position);
	goto(WingedG.position.x + 10, WingedG.position.y);
	
	turn(direction(WingedG.position));
	turn(180); 
	move(-7.5);
}
