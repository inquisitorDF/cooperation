public class Fly extends Robot {
	    void returnCell(int name) {
		        goto(this.getSavedX(), this.getSavedY(), 4);
		        turn(direction(radar(name).position));
		        drop();
		    }
	
	    void checkTemp(object name) {
		        while(name.temperature > 0.3) {
			            wait(0.2);
			        }
		    }
	
	    void placeItem(point neighbourhood, int minr, int maxr, float height) {
		        goto(space(neighbourhood, minr, maxr), height);
		        drop();
		    }
	
	    void goFor(int target, point neighbourhood, int dirx, int diry, float height) {
		        goto(search(target, neighbourhood.x + dirx, neighbourhood.y + diry).position, height);
		        grab();
		    }
}

extern void object::fly1_oo()
{
	errmode(0);
	
	    Flags flags = new Flags();
	    Fly Fly1 = new Fly;
	    Points poi = new Points();
	    
	Fly1.savePosition(position);
	Fly1.takeOutCell();
	Fly1.waitForCompanion(WheeledShooter, 5);
	Fly1.stealCell();
	//Fly1.takeOff(2);
	    Fly1.goFor(PowerCell, flags.yellow.position, 0, -5, 4);
	    object wgrabb = radar(WheeledGrabber);
	
	goto(wgrabb.position.x + 10, wgrabb.position.y);
	
	turn(direction(wgrabb.position));
	
	move(6.5);
	    drop();
	
	    Fly1.goFor(NuclearCell, flags.red.position, 0, 0, 2);
	    Fly1.placeItem(flags.violet.position, 0, 7, 4);
	    goto(space(flags.green.position, 0, 10), 2);
	    
	while(true) {
		        goto(space(flags.green.position, 0, 5, 2));
		        if(distance(search(NuclearCell, flags.green.position).position, flags.green.position) < 30 || distance(search(NuclearCell, poi.pointX).position, poi.pointX) < 20) {
			            Fly1.goFor(NuclearCell, flags.green.position, 0, 0, 2);
			        } else {
			            break;            
			        }
		
		        Fly1.checkTemp(this);
		        Fly1.placeItem(flags.violet.position, 0, 7, 2);
		        Fly1.checkTemp(this);
		    }
	
	    while(true) {
		        goto(space(poi.pointZ, 0, 5, 2));
		            if(distance(search(Titanium, poi.pointX).position, poi.pointX) < 15) {
			                Fly1.goFor(Titanium, poi.pointX, 0, 0, 0);
			            } else {
			                continue;
			            }
		
		        Fly1.checkTemp(this);
		        Fly1.placeItem(flags.violet.position, 0, 7, 2);
		        Fly1.checkTemp(this);
		    }
}
