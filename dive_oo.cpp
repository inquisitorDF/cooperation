public class Dive extends Robot {
	    void waitForLeaving(int name, float distance) {
		        object companion = radar(name);
		        while(true) {
			            companion = radar(name, 0, 90, 0, 30);
			            if(companion != null) {
				                if(distance(this.position, companion.position) > distance) {
					                    wait(2.3);
					                    break;
					                }
				            }
			        }
		    }
}

extern void object::dive_oo()
{
	errmode(0);
	    
	    Flags flags = new Flags();
	    Dive sub = new Dive;
	    Points poi = new Points();
	
	object spider = radar(AlienSpider, 0, 360, 20, 500, -1);
	
	sub.savePosition(spider.position);
	    sub.waitForLeaving(WheeledShooter, 25);
	    sub.goFor(NuclearCell, flags.red.position, 0, 0);
	
	object wheel = radar(WheeledGrabber);
	object fly = search(WingedGrabber, wheel.position.x, wheel.position.y);
	
	goto(fly.position.x + 10, fly.position.y);
	wait(0.1);
	
	turn(direction(fly.position));
	turn(180);
	//drop();
	move(-7.1);
	
	
	while(true) {
		object cell = radar(PowerCell);
		if(distance(position, cell.position) < 5) {
			wait(6);
			break;
		}
	}
	
	goto(sub.getSavedX(), sub.getSavedY());
	sub.placeItem(flags.green.position, 5, 15);
	//sub.goFor(TitaniumOre, flags.yellow.position, 0, 0);
	
	for(int i = 0; i < 5; i++) {
		
		
		if(i == 4) {
			break;
		}
		
		
		//goto(space(flags.green.position));
		object target = radar(NuclearCell, 0, 360, 10, 70, -1);
		// object target = search(NuclearCell, flags.green.position);
		if(target != null && distance(target.position, flags.green.position) > 20) {
			goto(target.position);
			grab();
			sub.placeItem(flags.green.position, 5, 15);
		} else if (i < 3) {
			target = radar(TitaniumOre, 0, 360, 5, 50, -1);
			goto(target.position);
			grab();
			sub.placeItem(poi.pointY, 2, 6);
		} 
	}
	
	while(true)
	{
		goto(space(flags.green.position, 1, 10, 2));
		
		object targetTitOre = radar(TitaniumOre, 0, 360, 1, 30, 1);
		
		while(targetTitOre.position.x < poi.pointY.x + 10)
		{
			goto(space(flags.green.position, 1, 10, 2));
			targetTitOre = radar(TitaniumOre, 0, 360, 2, 100, 1);
		} 
		
		object lastCell = radar(NuclearCell, 0, 360, 1, 30, 1);
		goto(lastCell.position);
		grab();
		sub.placeItem(flags.violet.position, 0, 17);
		break;
		
		if(targetTitOre != null) {
			goto(targetTitOre.position);
			grab();
			goto(space(poi.pointY, 0, 5, 0));
			drop();
		} else {
			
		}
	}
	goto(flags.green.position);
	drop();
	move(-10);
	/*while(true) {
		if(distance(search(Titanium, flags.green.position).position, flags.green.position) > 20) {
			wait(2);
		} else {
			break;
		}
	}*/
	
}
